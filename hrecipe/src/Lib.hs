module Lib
    ( startApp
    ) where

import qualified Business.Model.Book         as B
import           Business.Model.Parser.Book
import qualified Business.Model.Renderer.DSL as DSL
import           Data.Maybe                  (fromJust)
import qualified Text.Megaparsec             as Megaparsec


src :: String
src = "\
    \recipe: Creamed Cucumbers @creamed-cucumbers   # polop\n\
    \  servings: 8\n\
    \  #servings: 5\n\
    \  source: paf # et rpaf\n\
    \  durations:\n\
    \    preparing: 00:45 # 45 min   \n\
    \    cooking: 1h # 60 min\n\
    \    preparing: 02:00 \n\
    \  tags: # grouick\n\
    \    - cuisine:française # paf\n\
    \    - type:accompagnement # greu\n\
    \\n\
    \  |>\n\
    \    <375g> de lard fumé @viande.porc.lard|fumé\n\
    \    <1> gros oignons @légume.oignon\n\
    \    <1kg> de foies de volaille @viande.volaille.foie\n\
    \    Faire revenir doucement le lard, l'oignon et le foie coupé en lamelle dans\n\
    \    une casserolle pendant [10min].\n\
    \\n\
    \\n\
    \  |>\n\
    \    <375g> de crème fouettée\n\
    \    <5cl> de cognac\n\
    \    Une fois cuit, mixer très fin et laisser refroidir avant d'y rajouter\n\
    \    le cognac et la crème fouettée.\n\
    \\n\
    \  |>\n\
    \    Mettre dans un plat au réfrigérateur pendant au moins [6h].\n\
    \"

book :: Maybe B.Book
book = Megaparsec.parseMaybe bookP src

startApp :: IO ()
startApp = do
    putStrLn $ DSL.renderBook (fromJust book)
