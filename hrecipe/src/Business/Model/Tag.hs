{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}
module Business.Model.Tag
  ( Tag(..),
  ) where

import           GHC.Generics

newtype Tag = Tag
  { value :: String
  } deriving (Show, Read, Generic, Eq, Ord)
