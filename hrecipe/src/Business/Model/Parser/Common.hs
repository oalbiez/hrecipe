{-# LANGUAGE DataKinds        #-}
{-# LANGUAGE FlexibleContexts #-}

module Business.Model.Parser.Common
  ( Parser
  , ParseError
  , strip
  , scn, sc
  , lexeme, symbol, keyword
  , colon, at, minus
  , integer, float
  , restOfLine, restOfLineWithReference
  ) where

import           Control.Applicative        hiding (some)
import           Control.Monad              (void)
import           Data.Void                  (Void)
import qualified Text.Megaparsec            as M
import qualified Text.Megaparsec.Char       as M
import qualified Text.Megaparsec.Char.Lexer as L


strip :: String -> String
strip = unwords . words

type Parser = M.Parsec Void String
type ParseError = M.ParseErrorBundle String Void

lineComment :: Parser ()
lineComment = L.skipLineComment "#"

scn :: Parser ()
scn = L.space M.space1 lineComment M.empty

sc :: Parser ()
sc = L.space (void $ M.some (M.char ' ' <|> M.char '\t')) lineComment empty

lexeme :: Parser a -> Parser a
lexeme = L.lexeme sc

symbol :: String -> Parser ()
symbol v = void $ L.symbol sc v

colon :: Parser ()
colon = symbol ":"

minus :: Parser ()
minus = symbol "-"

at :: Parser ()
at = symbol "@"

keyword :: String -> Parser ()
keyword value = symbol value <* colon

integer :: Parser Int
integer = lexeme L.decimal

float :: Parser Float
float = lexeme $ M.choice
    [ M.try L.float
    , M.try (fromIntegral <$> L.decimal)
    ]

restOfLine :: Parser String
restOfLine = strip <$> lexeme (M.takeWhile1P (Just "line") (`notElem` "\n\r#"))

restOfLineWithReference :: Parser (String, Maybe String)
restOfLineWithReference = do
    v <- strip <$> lexeme (M.takeWhile1P (Just "text") (`notElem` "\n\r@#"))
    r <- referenceP
    pure ( v, r)
    where
        referenceP :: Parser (Maybe String)
        referenceP = M.optional (do
            at
            strip <$> lexeme (M.some (M.alphaNumChar <|> M.char '-' <|> M.char '_' <|> M.char '.' <|> M.char '/' <|> M.char '|')) M.<?> "reference")
