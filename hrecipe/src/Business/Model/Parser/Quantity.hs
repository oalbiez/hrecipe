{-# LANGUAGE DataKinds        #-}
{-# LANGUAGE FlexibleContexts #-}

module Business.Model.Parser.Quantity
  ( timeP
  , massP
  , temperatureP
  , volumeP
  , scalarP
  ) where

import           Business.Model.Parser.Common
import           Business.Model.Quantities.Duration    (Duration, hhmm, hours, minutes)
import qualified Business.Model.Quantities.Mass        as Q
import qualified Business.Model.Quantities.Scalar      as Q
import qualified Business.Model.Quantities.Temperature as Q
import qualified Business.Model.Quantities.Volume      as Q
import           Data.Function                         ((&))
import qualified Text.Megaparsec                       as M
import qualified Text.Megaparsec.Char                  as M


timeP :: Parser Duration
timeP = lexeme $ M.choice
    [ M.try (minutes <$> (integer <* symbol "min"))
    , M.try (hours <$> (float <* symbol "h"))
    , M.try $ do
        hh <- M.count 2 M.digitChar
        _ <- M.char ':'
        mm <- M.count 2 M.digitChar
        pure (hhmm (read hh) (read mm))
    ]

massP :: Parser Q.Mass
massP = (&) <$> float <*> unitP
    where
        unitP = M.choice
            [ do { symbol "kg"; pure Q.kg }
            , do { symbol "g"; pure Q.g }
            , do { symbol "mg"; pure Q.mg }
            ]

temperatureP :: Parser Q.Temperature
temperatureP = (&) <$> float <*> unitP
    where
        unitP = M.choice
            [ do { symbol "°C"; pure Q.celsius }
            , do { symbol "°F"; pure Q.fahrenheit }
            ]

volumeP :: Parser Q.Volume
volumeP = (&) <$> float <*> unitP
    where
        unitP = M.choice
            [ do { symbol "l"; pure Q.l }
            , do { symbol "dl"; pure Q.dl }
            , do { symbol "cl"; pure Q.cl }
            , do { symbol "ml"; pure $ Q.ml . round }
            , do { symbol "tsp"; pure $ Q.tsp . round }
            , do { symbol "tbs"; pure $ Q.tbs . round }
            ]

scalarP :: Parser Q.Scalar
scalarP = Q.scalar <$> float
