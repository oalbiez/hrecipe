
    book
      lang: fr  -> set comma, temperature, some units (tsp, cac, ...)
      servings: 6 -> normalize all recipes to 6 servings

    aliment: ...
      ...

    pantry: path/to/file
    include: path/to/file

    recipe: Paté foie de volaille @pate-foie-volaille
      serving: 4                                    # ok
      source: unknow                                # ok
      tags: cuisine:française type:accompagnement   # ok
        cuisine:française
        type:accompagnement
      durations:                                    # ok
        preparing: 15min
        backing: 30min

      |>
        <375g> de lard fumé @viande.porc.lard|fumé
        <1> gros oignons @légume.oignon
        <1kg> de foies de volaille @viande.volaille.foie
        Faire revenir doucement le lard, l'oignon et le foie coupé en lamelle dans
        une casserolle pendant [10min].

      |>
        <375g> de crème fouettée
        <5cl> de cognac
        Une fois cuit, mixer très fin et laisser refroidir avant d'y rajouter
        le cognac et la crème fouettée.

      |>
        Mettre dans un plat au réfrigérateur pendant au moins [6h].


## notes
- <v> : a value which should scale depending on servings
- [v] : a simple value with unit depending of context
- @v : a reference / identifier
