{-# LANGUAGE DataKinds        #-}
{-# LANGUAGE FlexibleContexts #-}

module Business.Model.Parser.Book
  ( qtyP
  , ingredientP
  , servingsP
  , sourceP
  , tagsP
  , durationsP
  , stepP
  , recipeP
  , bookP
  ) where

import qualified Business.Model.Book            as B
import qualified Business.Model.Ingredient      as I
import           Business.Model.Parser.Common
import           Business.Model.Parser.Quantity
import qualified Business.Model.Recipe          as R
import qualified Business.Model.Step            as S
import           Business.Model.Tag             (Tag (..))
import           Data.Function                  ((&))
import qualified Data.Map                       as Map
import qualified Text.Megaparsec                as M
import qualified Text.Megaparsec.Char.Lexer     as L

apply :: [a -> a] -> a -> a
apply = foldr (.) id


-- Ingredients

qtyP :: Parser I.Qty
qtyP = M.between (symbol "<") (symbol ">") $ M.choice
    [ M.try (I.Mass <$> massP)
    , M.try (I.Volume <$> volumeP)
    , M.try (I.Piece <$> scalarP)
    ]

ingredientP :: Parser I.Ingredient
ingredientP = do
    qty <- qtyP
    (label, ref) <- restOfLineWithReference
    pure $ I.Ingredient ref label qty


-- Recipe metadata

servingsP :: Parser Int
servingsP = keyword "servings" *> integer

sourceP :: Parser String
sourceP = keyword "source" *> restOfLine

tagsP :: Parser [Tag]
tagsP = L.indentBlock scn blockP
  where
    blockP = do
        keyword "tags"
        pure (L.IndentMany Nothing pure innerBlockP)
    innerBlockP = Tag <$> (minus *> restOfLine)

durationsP :: Parser R.Durations
durationsP = L.indentBlock scn blockP
  where
    blockP = do
        keyword "durations"
        pure (L.IndentMany Nothing (pure . toMap . reverse) innerBlockP)

    innerBlockP = uncurry Map.insert <$> ((,) <$> durationSpecificationP <*> timeP)

    toMap fct = apply fct Map.empty

    durationSpecificationP = M.choice
        [ do { keyword "waiting"; pure R.Waiting}
        , do { keyword "preparing"; pure R.Preparing}
        , do { keyword "cooking"; pure R.Cooking}
        , do { keyword "baking"; pure R.Baking}
        ]

-- Recipe

stepP :: Parser S.Step
stepP = L.indentBlock scn blockP
  where
    blockP = do
        symbol "|>"
        pure (L.IndentMany Nothing (pure . toStep . reverse) innerBlockP)

    innerBlockP = M.choice
        [ S.addIngredient <$> ingredientP
        , S.addParts <$> partsP
        ]

    partsP = M.some partP
    partP = M.choice
        [ M.try stepQtyP
        , M.try stepTextP
        ]
    stepTextP = S.Text . strip <$> lexeme (M.takeWhile1P (Just "line") (`notElem` "\n\r#[]"))
    stepQtyP = M.between (symbol "[") (symbol "]") $ M.choice
        [ M.try (S.D <$> timeP)
        , M.try (S.T <$> temperatureP)
        , M.try (S.M <$> massP)
        , M.try (S.V <$> volumeP)
        , M.try (S.P <$> scalarP)
        ]

    toStep actions = apply actions S.empty

recipeP :: Parser R.Recipe
recipeP = L.nonIndented scn (L.indentBlock scn blockP)
    where
        blockP = do
            keyword "recipe"
            (name, ref) <- restOfLineWithReference
            let builder = R.create name ref & flip apply
            pure (L.IndentMany Nothing (pure . builder . reverse) innerBlockP)

        innerBlockP = M.choice
            [ M.try $ servings <$> servingsP
            , M.try $ source <$> sourceP
            , M.try $ durations <$> durationsP
            , M.try $ tags <$> tagsP
            , M.try $ step <$> stepP
            ]

        servings value recipe = recipe { R.serving = value }
        source value recipe = recipe { R.source = value }
        durations values recipe = recipe { R.durations = values }
        tags values recipe = recipe { R.tags = values }
        step value = R.addStep value

bookP :: Parser B.Book
bookP = do
    v <- M.many recipeP
    _ <- M.eof
    pure $ B.Book v
