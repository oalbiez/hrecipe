{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE QuasiQuotes   #-}

module Business.Model.Quantities.Scalar
  ( Scalar(..)
  , piece
  , scalar
  , renderScalar
  ) where

import           Business.Model.Quantities.Dimension (Dimension (..), ValueUnit (ValueUnit))
import           Data.Function                       ((&))
import           Data.String.Interpolate             (i)
import           GHC.Generics


newtype Scalar = Scalar Float deriving (Show, Read, Generic, Eq, Ord)

instance Dimension Scalar where
    render = renderScalar
    describe (Scalar v )= ValueUnit v "#"
    (Scalar x) ^+^ (Scalar y) = Scalar (x + y)
    (Scalar x) ^-^ (Scalar y) = Scalar (x - y)
    (Scalar x) ^* y = x * y & Scalar
    (Scalar x) ^/ y = x / y & Scalar


piece :: Float -> Scalar
piece = Scalar

scalar :: Float -> Scalar
scalar = Scalar

renderScalar :: Scalar -> String
renderScalar (Scalar v) = [i|#{v}|]
