{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE QuasiQuotes   #-}

module Business.Model.Quantities.Energy
  ( Energy(..)
  , kcal, j
  , toKcal, toJoules
  ) where

import           Business.Model.Quantities.Dimension (Dimension (..), ValueUnit (ValueUnit))
import           Data.Function                       ((&))
import           Data.String.Interpolate             (i)
import           GHC.Generics


newtype Energy
    = Joules Float
    deriving (Show, Read, Generic, Eq, Ord)

instance Dimension Energy where
    render (Joules v) = [i|#{v} J|]
    describe (Joules v) = ValueUnit v "J"
    (Joules x) ^+^ (Joules y) = Joules (x + y)
    (Joules x) ^-^ (Joules y) = Joules (x - y)
    (Joules x) ^* y = x * y & Joules
    (Joules x) ^/ y = x / y & Joules


kcal :: Float -> Energy
kcal v = v * 4184 & Joules

j :: Float -> Energy
j = Joules

toKcal :: Energy -> Float
toKcal (Joules v) = v / 4184

toJoules :: Energy -> Float
toJoules (Joules v) = v
