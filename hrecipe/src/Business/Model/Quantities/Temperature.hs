{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE QuasiQuotes   #-}

module Business.Model.Quantities.Temperature
  ( Temperature(..)
  , celsius, fahrenheit
  , toCelsius, toFahrenheit
  ) where

import           Business.Model.Quantities.Dimension (Dimension (..), ValueUnit (ValueUnit))
import           Data.Function                       ((&))
import           Data.String.Interpolate             (i)
import           GHC.Generics


newtype Temperature
    = Degree Float
    deriving (Show, Read, Generic, Eq, Ord)

instance Dimension Temperature where
    render (Degree v) = [i|#{v}°C|]
    describe (Degree v) = ValueUnit v "°C"
    (Degree x) ^+^ (Degree y) = Degree (x + y)
    (Degree x) ^-^ (Degree y) = Degree (x - y)
    (Degree x) ^* y = x * y & Degree
    (Degree x) ^/ y = x / y & Degree

celsius :: Float -> Temperature
celsius = Degree

toCelsius :: Temperature -> Int
toCelsius (Degree v) = v & round

fahrenheit :: Float -> Temperature
fahrenheit v = Degree $ (v - 32) / 1.8

toFahrenheit :: Temperature -> Int
toFahrenheit (Degree v) = v * 1.8 + 32 & round
