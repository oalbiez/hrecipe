module Business.Model.Quantities.Dimension
  ( Dimension(..)
  , ValueUnit(..)
  ) where


data ValueUnit = ValueUnit Float String deriving (Show, Read)


class Ord d => Dimension d where
    render :: d -> String
    describe :: d -> ValueUnit
    (^+^):: d -> d -> d
    (^-^):: d -> d -> d
    (^*):: d -> Float -> d
    (^/):: d -> Float -> d
