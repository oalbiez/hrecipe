{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE QuasiQuotes   #-}

module Business.Model.Quantities.Mass
  ( Mass(..)
  , kg, g, mg, ug, µg
  ) where

import           Business.Model.Quantities.Dimension (Dimension (..), ValueUnit (ValueUnit))
import           Data.Function                       ((&))
import           Data.String.Interpolate             (i)
import           GHC.Generics

newtype Mass
    = MicroGram Int
    deriving (Show, Read, Generic, Eq, Ord)


instance Dimension Mass where
    render = renderMass
    describe = describeMass
    (MicroGram x) ^+^ (MicroGram y) = MicroGram (x + y)
    (MicroGram x) ^-^ (MicroGram y) = MicroGram (x - y)
    (MicroGram x) ^* y = fromIntegral x * y & round & MicroGram
    (MicroGram x) ^/ y = fromIntegral x / y & round & MicroGram


kg :: Float -> Mass
kg v = v * 1000000000 & round & MicroGram

g :: Float -> Mass
g v = v * 1000000 & round & MicroGram

mg :: Float -> Mass
mg v = v * 1000 & round & MicroGram

µg :: Int -> Mass
µg = MicroGram

ug :: Int -> Mass
ug = MicroGram

toKiloGram :: Mass -> Float
toKiloGram m = toGram m / 1000

toGram :: Mass -> Float
toGram m = toMilliGram m  / 1000

toMilliGram :: Mass -> Float
toMilliGram m = value / 1000
                where value = fromIntegral (toMicroGram m)

toMicroGram :: Mass -> Int
toMicroGram (MicroGram v) = v


describeMass :: Mass -> ValueUnit
describeMass (MicroGram v)  | v `mod` 1000000000 == 0 = ValueUnit (toKiloGram (MicroGram v)) "kg"
                            | v `mod` 1000000 == 0 = ValueUnit (toGram (MicroGram v)) "g"
                            | v `mod` 1000 == 0 = ValueUnit (toMilliGram (MicroGram v)) "mg"
                            | otherwise = ValueUnit (fromIntegral v) "µg"

renderMass :: Mass -> String
renderMass m = let (ValueUnit magnitude u) = describeMass m
                in [i|#{magnitude} #{u}|]
