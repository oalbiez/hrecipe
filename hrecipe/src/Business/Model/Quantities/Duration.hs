{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE QuasiQuotes   #-}

module Business.Model.Quantities.Duration
  ( Duration(..)
  , minutes, hours, hhmm
  , toMinutes
  ) where

import           Business.Model.Quantities.Dimension (Dimension (..), ValueUnit (ValueUnit))
import           Data.Function                       ((&))
import           Data.String.Interpolate             (i)
import           GHC.Generics


newtype Duration
    = Minutes Int
    deriving (Show, Read, Generic, Eq, Ord)


instance Dimension Duration where
    render (Minutes v) = [i|#{v} min|]
    describe (Minutes v) = ValueUnit (fromIntegral v) "min"
    (Minutes x) ^+^ (Minutes y) = Minutes (x + y)
    (Minutes x) ^-^ (Minutes y) = Minutes (x - y)
    (Minutes x) ^* y = fromIntegral x * y & round & Minutes
    (Minutes x) ^/ y = fromIntegral x / y & round & Minutes


minutes :: Int -> Duration
minutes = Minutes

hours :: Float -> Duration
hours v = v * 60 & round & Minutes

hhmm :: Int -> Int -> Duration
hhmm h m = h * 60 + m & minutes

toMinutes :: Duration -> Int
toMinutes (Minutes v) = v
