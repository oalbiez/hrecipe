{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE QuasiQuotes   #-}

module Business.Model.Quantities.Volume
  ( Volume(..)
  , l, dl, cl, ml, tbs, tsp
  ) where

import           Business.Model.Quantities.Dimension (Dimension (..), ValueUnit (ValueUnit))
import           Data.Function                       ((&))
import           Data.String.Interpolate             (i)
import           GHC.Generics


newtype Volume
    = MilliLitre Int
    deriving (Show, Read, Generic, Eq, Ord)


instance Dimension Volume where
    render = renderVolume
    describe = describeVolume
    (MilliLitre x) ^+^ (MilliLitre y) = MilliLitre (x + y)
    (MilliLitre x) ^-^ (MilliLitre y) = MilliLitre (x - y)
    (MilliLitre x) ^* y = fromIntegral x * y & round & MilliLitre
    (MilliLitre x) ^/ y = fromIntegral x / y & round & MilliLitre


l :: Float -> Volume
l v = v * 1000 & round & MilliLitre

dl :: Float -> Volume
dl v = v * 100 & round & MilliLitre

cl :: Float -> Volume
cl v = v * 10 & round & MilliLitre

ml :: Int -> Volume
ml = MilliLitre

tbs :: Int -> Volume
tbs v = v * 15 & MilliLitre

tsp :: Int -> Volume
tsp v = v * 5 & MilliLitre

toL :: Volume -> Float
toL (MilliLitre v) = f / 1000 where f = fromIntegral v


describeVolume :: Volume -> ValueUnit
describeVolume (MilliLitre v)| v == 5 = ValueUnit 1 "tsp"
                            | v == 10 = ValueUnit 2 "tsp"
                            | v == 15 = ValueUnit 1 "tbs"
                            | v == 20 = ValueUnit 4 "tsp"
                            | v == 25 = ValueUnit 5 "tsp"
                            | v == 30 = ValueUnit 2 "tbs"
                            | v == 45 = ValueUnit 3 "tbs"
                            | v == 60 = ValueUnit 4 "tbs"
                            | v == 75 = ValueUnit 5 "tbs"
                            | v >= 1000 = ValueUnit (toL (MilliLitre v)) "l"
                            | v `mod` 10 == 0 = ValueUnit (fromIntegral (v `div` 10)) "cl"
                            | otherwise = ValueUnit (fromIntegral v) "ml"

renderVolume :: Volume -> String
renderVolume v = let (ValueUnit magnitude u) = describeVolume v
                in [i|#{magnitude} #{u}|]
