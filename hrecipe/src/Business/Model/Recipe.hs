{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}

module Business.Model.Recipe
  ( Recipe (..)
  , DurationSpecification(..)
  , Durations
  , create
  , addDuration
  , addStep
  , addTag
  , scaleServing
  ) where

import           Business.Model.Quantities.Duration (Duration)
import           Business.Model.Step                (Step (..), scaleStep)
import           Business.Model.Tag                 (Tag)
import           Data.Function                      ((&))
import qualified Data.Map                           as Map
import           GHC.Generics


data DurationSpecification
    = Waiting
    | Preparing
    | Cooking
    | Baking
    deriving (Show, Read, Generic, Eq, Ord)


type Durations = Map.Map DurationSpecification Duration

data Recipe = Recipe
  { identifier :: Maybe String
  , name       :: String
  , source     :: String
  , serving    :: Int
  , durations  :: Map.Map DurationSpecification Duration
  , tags       :: [Tag]
  , steps      :: [Step]
  } deriving (Show, Read, Generic, Eq)

create :: String -> Maybe String -> Recipe
create name identifier = Recipe
    { identifier  = identifier
    , name        = name
    , source      = ""
    , serving     = 0
    , durations   = Map.empty
    , tags        = []
    , steps       = []
    }

addTag :: Tag -> Recipe -> Recipe
addTag tag recipe = recipe { tags = tags recipe ++ [tag] }

addDuration :: DurationSpecification -> Duration -> Recipe -> Recipe
addDuration name duration recipe = recipe { durations = durations recipe & Map.insert name duration }

addStep :: Step -> Recipe -> Recipe
addStep step recipe = recipe { steps = steps recipe ++ [step] }

scaleServing :: Int -> Recipe -> Recipe
scaleServing newServing recipe
    = let factor :: Float
          factor = fromIntegral newServing / fromIntegral (serving recipe)
    in recipe { serving = newServing
              , steps = steps recipe & map (scaleStep factor)
              }
