{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes      #-}
module Business.Model.Renderer.Latex
    ( renderBook
    , renderIngredient
    , renderDimension
    , renderQuantity
    , renderRecipe
    , renderStep
    ) where

import           Business.Model.Book                   (Book (..))
import           Business.Model.Ingredient             (Ingredient (..), Qty (..))
import           Business.Model.Quantities.Dimension   (Dimension (..), ValueUnit (..))
import           Business.Model.Quantities.Duration    (Duration, toMinutes)
import           Business.Model.Quantities.Temperature (toCelsius, toFahrenheit)
import           Business.Model.Recipe                 (DurationSpecification (..), Recipe (..))
import           Business.Model.Renderer.Context       (Context (..), TemperatureRender (..))
import           Business.Model.Step                   (Part (..), Step (..))
import           Business.Model.Tag                    (Tag (..))
import           Data.Function                         ((&))
import           Data.List                             (intercalate)
import qualified Data.Map                              as Map
import           Data.String.Interpolate               (i)


header :: String
header = "\
    \\\documentclass[12pt]{article}\n\
    \\\usepackage[utf8]{inputenc}\n\
    \\\usepackage[french]{babel}\n\
    \\\usepackage[a4paper,textwidth=18cm,textheight=27cm]{geometry}\n\
    \\\usepackage[nonumber]{cuisine}\n\
    \\\usepackage{cooking-units}\n\
    \\\usepackage{fontawesome}\n\
    \\\usepackage{tikzsymbols}\n\
    \\n\
    \\\pagenumbering{gobble}\n\
    \\n\
    \\\newcommand{\\preparing}[2]{\\hspace{0.25cm} {\\faGears} {\\bfseries{\\cunum{#1}{#2}}}}\n\
    \\\newcommand{\\cooking}[2]{\\hspace{0.25cm} {\\fryingpan} {\\bfseries{\\cunum{#1}{#2}}}}\n\
    \\\newcommand{\\baking}[2]{\\hspace{0.25cm} {\\oven} {\\bfseries{\\cunum{#1}{#2}}}}\n\
    \\\newcommand{\\waiting}[2]{\\hspace{0.25cm} {attente} {\\bfseries{\\cunum{#1}{#2}}}}\n\
    \\\newcommand{\\serving}[1]{{\\faUser} #1}\n\
    \\\newcommand{\\tags}[1]{\\freeform #1}\n\
    \\\newcommand{\\tag}[1]{{\\small \\faHashtag}{\\bfseries #1}}\n\
    \\n\
    \\\RecipeWidths{\\textwidth}{3cm}{1cm}{4cm}{.75cm}{.75cm}\n\
    \\\renewcommand*{\\recipefont}{\\Large\\sffamily}\n\
    \\\renewcommand*{\\recipefreeformfont}{\\itshape}\n\
    \\\renewcommand*{\\recipeingredientfont}{\\large\\sffamily}\n\
    \\\renewcommand*{\\recipestepnumberfont}{\\LARGE\\bfseries\\sffamily}\n\
    \\\renewcommand*{\\recipetitlefont}{\\Huge\\selectfont\\bfseries\\sffamily}\n\
    \\n\
    \\\newcookingunit[c.à.s]{cas}\n\
    \\\newcookingunit[c.à.c]{cac}\n\
    \\n"


renderBook :: Context -> Book -> String
renderBook ctx book
    = let recpiesSection = recipes book & map (renderRecipe ctx) & intercalate "\\newpage\n"
        in header
        ++ "\\begin{document}\n"
        ++ recpiesSection
        ++ "\\end{document}\n"


renderDurations :: Context -> DurationSpecification -> Duration -> String
renderDurations _ specification duration =
    case specification of
        Waiting   -> [i|\\waiting{#{toMinutes duration}}{min}|]
        Preparing -> [i|\\preparing{#{toMinutes duration}}{min}|]
        Cooking   -> [i|\\cooking{#{toMinutes duration}}{min}|]
        Baking    -> [i|\\baking{#{toMinutes duration}}{min}|]


renderRecipe :: Context -> Recipe -> String
renderRecipe ctx recipe =
    let
        stepsSection = steps recipe & map (renderStep ctx) & intercalate "\n"
        durationsSection = durations recipe & Map.mapWithKey (renderDurations ctx) & Map.elems & unwords
        tagsSection = tags recipe & map renderTag & unwords
    in [i|\\begin{recipe}{#{name recipe}}{\\serving{#{serving recipe}}}{#{durationsSection}}\n|]
        ++ [i|\\tags{#{tagsSection}}\n|]
        ++ stepsSection
        ++ "\\end{recipe}\n"


renderIngredient :: Context -> Ingredient -> String
renderIngredient ctx ingredient =
    [i|\\Ingredient{#{qty ingredient & renderQuantity ctx} #{label ingredient}}\n|]

renderStep :: Context -> Step -> String
renderStep ctx step =
    let
        renderPart (Text v) = v
        renderPart (D v)    = [i|\\cunum{#{toMinutes v}}{min}|]
        renderPart (T v)    =
            case temperature ctx of
                Celcius    -> [i|\\cunum{#{toCelsius v}}{C}|]
                Fahrenheit -> [i|\\cunum{#{toFahrenheit v}}{F}|]
        renderPart (M v)  = renderDimension ctx v
        renderPart (V v)  = renderDimension ctx v
        renderPart (P v)  = renderDimension ctx v
        ingredientsSection = ingredients step & concatMap (renderIngredient ctx)
        descriptionSection = parts step & map renderPart & unwords

    in if null (ingredients step)
        then "\\newstep\n" ++ descriptionSection ++ "\n"
        else ingredientsSection ++ descriptionSection ++ "\n"


renderTag :: Tag -> String
renderTag tag = [i|\\tag{#{value tag}}|]

renderQuantity :: Context -> Qty -> String
renderQuantity ctx value =
    case value of
        (Piece s)  -> renderDimension ctx s
        (Mass m)   -> renderDimension ctx m
        (Volume v) -> renderDimension ctx v

renderDimension :: Dimension d => Context -> d -> String
renderDimension _ value =
    let ValueUnit magnitude unit = describe value
    in if unit == "#"
        then [i|\\cuam{#{magnitude}}|]
        else [i|\\cunum{#{magnitude}}{#{unit}}|]
