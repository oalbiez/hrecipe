module Business.Model.Renderer.Context (TemperatureRender(..), Context(..), fr, en)where

data TemperatureRender = Celcius | Fahrenheit

data Context
    = Context
    { lang        :: String
    , coma        :: String
    , temperature :: TemperatureRender
    }

fr :: Context
fr = Context
    { lang = "FR"
    , coma = ","
    , temperature = Celcius
    }

en :: Context
en = Context
    { lang = "FR"
    , coma = ","
    , temperature = Fahrenheit
    }
