{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes      #-}
module Business.Model.Renderer.DSL
    ( renderBook
    , renderIngredient
    , renderDimension
    , renderQuantity
    , renderRecipe
    , renderStep
    ) where

import qualified Business.Model.Book                   as B
import qualified Business.Model.Ingredient             as I
import           Business.Model.Quantities.Dimension   (Dimension (..), ValueUnit (..))
import qualified Business.Model.Quantities.Duration    as Q
import qualified Business.Model.Quantities.Temperature as Q
import qualified Business.Model.Recipe                 as R
import qualified Business.Model.Step                   as S
import qualified Business.Model.Tag                    as T
import           Data.Function                         ((&))
import           Data.List                             (intercalate)
import qualified Data.Map                              as Map
import           Data.String.Interpolate               (i)
import           Data.Text                             (Text, pack, unpack)
import           NeatInterpolation

renderBook :: B.Book -> String
renderBook book
    = B.recipes book & map renderRecipe & intercalate "\n\n"

renderRecipe :: R.Recipe -> String
renderRecipe recipe =
    let
        name :: Text
        name = R.name recipe & pack
        identifier :: Text
        identifier = R.identifier recipe & renderIdentifier & pack
        source :: Text
        source = R.source recipe & pack
        servings :: Text
        servings = R.serving recipe & show & pack
        durationsSection :: Text
        durationsSection
            = R.durations recipe
            & Map.mapWithKey renderDurations
            & Map.elems
            & intercalate "\n"
            & pack
        tagsSection :: Text
        tagsSection
            = R.tags recipe
            & map renderTag
            & intercalate "\n"
            & pack
        stepsSection :: Text
        stepsSection
            = R.steps recipe
            & map renderStep
            & intercalate "\n\n"
            & pack
    in
        [trimming|
        recipe: ${name} ${identifier}
            source: ${source}
            servings: ${servings}
            tags:
                ${tagsSection}
            durations:
                ${durationsSection}

            ${stepsSection}
        |] & unpack

renderDurations :: R.DurationSpecification -> Q.Duration -> String
renderDurations specification duration =
    case specification of
        R.Waiting   -> [i|waiting: #{Q.toMinutes duration} min|]
        R.Preparing -> [i|preparing: #{Q.toMinutes duration} min|]
        R.Cooking   -> [i|cooking: #{Q.toMinutes duration} min|]
        R.Baking    -> [i|baking: #{Q.toMinutes duration} min|]

renderTag :: T.Tag -> String
renderTag tag = [i|- #{T.value tag}|]

renderStep :: S.Step -> String
renderStep step =
    let
        ingredientsSection = S.ingredients step & concatMap renderIngredient & pack
        renderPart (S.Text v) = v
        renderPart (S.D v)    = [i|[#{Q.toMinutes v} min]|]
        renderPart (S.T v)    = [i|[#{Q.toCelsius v}}°C]|]
        renderPart (S.M v)    = [i|[#{renderDimension v}]|]
        renderPart (S.V v)    = [i|[#{renderDimension v}]|]
        renderPart (S.P v)    = [i|[#{renderDimension v}]|]
        descriptionSection = S.parts step & map renderPart & unwords & pack

    in [trimming|
        |>
            ${ingredientsSection}
            ${descriptionSection}

        |] & unpack



renderIngredient :: I.Ingredient -> String
renderIngredient ingredient =
    let
        qty = I.qty ingredient & renderQuantity
        ref = I.aliment ingredient & renderIdentifier
    in [i|<#{qty}> #{I.label ingredient} #{ref}\n|]

renderQuantity :: I.Qty -> String
renderQuantity value =
    case value of
        (I.Piece s)  -> renderDimension s
        (I.Mass m)   -> renderDimension m
        (I.Volume v) -> renderDimension v

renderDimension :: Dimension d => d -> String
renderDimension value =
    let ValueUnit magnitude unit = describe value
    in if unit == "#"
        then [i|#{magnitude}|]
        else [i|#{magnitude} #{unit}|]

renderIdentifier :: Maybe String -> String
renderIdentifier value
    = value
    & maybe "" (\x -> [i|@#{x}|])
