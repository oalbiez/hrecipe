{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}
module Business.Model.Book
  ( Book(..)
  , empty
  , addRecipe
  , harmonizeServing
  ) where

import           Business.Model.Recipe
import           Data.Function         ((&))
import           GHC.Generics


newtype Book = Book
  { recipes :: [Recipe]
  } deriving (Show, Read, Generic)


empty :: Book
empty = Book { recipes = [] }

addRecipe :: Recipe -> Book -> Book
addRecipe recipe book = book { recipes = recipes book ++ [recipe] }


harmonizeServing :: Int -> Book -> Book
harmonizeServing servings book
    = let scale = scaleServing servings
    in book { recipes = recipes book & map scale }

