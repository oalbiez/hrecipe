{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}

module Business.Model.Ingredient
  ( Ingredient (..)
  , Qty(..)
  , scaleIngredient
  , parseQty
  ) where

import           Business.Model.Quantities.Dimension (Dimension (..))
import qualified Business.Model.Quantities.Mass      as Q
import qualified Business.Model.Quantities.Scalar    as Q
import qualified Business.Model.Quantities.Volume    as Q
import           Data.Char                           (toLower)
import           Data.Function                       ((&))
import           GHC.Generics


data Qty
    = Piece Q.Scalar
    | Mass Q.Mass
    | Volume Q.Volume
    deriving (Show, Read, Generic, Eq)


data Ingredient = Ingredient
  { aliment :: Maybe String
  , label   :: String
  , qty     :: Qty
  } deriving (Show, Read, Generic, Eq)


scaleIngredient :: Float -> Ingredient -> Ingredient
scaleIngredient factor ingredient
    = ingredient { qty = qty ingredient & scaleQty factor }
    where
        scaleQty :: Float -> Qty -> Qty
        scaleQty factor (Piece p)  = Piece (p ^* factor)
        scaleQty factor (Mass m)   = Mass (m ^* factor)
        scaleQty factor (Volume v) = Volume (v ^* factor)



parseQty :: String -> Qty
parseQty definition
    = let parts = words $ map toLower definition
      in case length parts of
          1 -> read (head parts) & Q.scalar & Piece
          2 -> let magnitude =  read (head parts):: Float
                in case parts!!1 of
                    "l"   -> magnitude & Q.l & Volume
                    "cl"  -> magnitude & Q.cl & Volume
                    "dl"  -> magnitude & Q.dl & Volume
                    "ml"  -> magnitude & round & Q.ml & Volume
                    "tsp" -> magnitude & round & Q.tsp & Volume
                    "cac" -> magnitude & round & Q.tsp & Volume
                    "tbs" -> magnitude & round & Q.tbs & Volume
                    "cas" -> magnitude & round & Q.tbs & Volume
                    "g"   -> magnitude & Q.g & Mass
                    "kg"  -> magnitude & Q.kg & Mass
                    "mg"  -> magnitude & Q.mg & Mass
                    _     -> error "Unknown unit"
          _ -> error "invalid Quantity definition"
