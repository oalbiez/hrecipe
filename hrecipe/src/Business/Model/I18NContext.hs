{-# LANGUAGE DataKinds        #-}
{-# LANGUAGE FlexibleContexts #-}

module Business.Model.I18NContext
  (
  ) where

-- import qualified Business.Model.Quantities.Mass     as M
-- import           Business.Model.Quantities.Quantity (Quantity (..))
-- import qualified Business.Model.Quantities.Scalar   as P
-- import qualified Business.Model.Quantities.Volume   as V


-- type UnitDefinition = [(String, Float -> Quantity)]

-- data I18NContext
--     = I18NContext
--     { lang  :: String
--     , comma :: Char
--     , units :: UnitDefinition
--     }



-- fr :: I18NContext
-- fr = I18NContext
--     { lang = "FR"
--     , comma = ','
--     , units =
--         [ ("l", Volume . V.l)
--         , ("dl", Volume . V.dl)
--         , ("cl", Volume . V.cl)
--         , ("ml", Volume . V.ml . round)
--         , ("kg", Mass . M.kg)
--         , ("g", Mass . M.g)
--         , ("mg", Mass . M.mg)
--         , ("cac", Volume . V.tsp . round)
--         , ("cas", Volume . V.tbs . round)
--         , ("", Piece . P.piece)
--         ]
--     }


-- en :: I18NContext
-- en = I18NContext
--     { lang = "EN"
--     , comma = '.'
--     , units =
--         [ ("l", Volume . V.l)
--         , ("dl", Volume . V.dl)
--         , ("cl", Volume . V.cl)
--         , ("ml", Volume . V.ml . round)
--         , ("kg", Mass . M.kg)
--         , ("g", Mass . M.g)
--         , ("mg", Mass . M.mg)
--         , ("tsp", Volume . V.tsp . round)
--         , ("tbs", Volume . V.tbs . round)
--         , ("", Piece . P.piece)
--         ]
--     }

