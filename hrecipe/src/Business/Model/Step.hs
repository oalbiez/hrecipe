{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}

module Business.Model.Step
  ( Step(..)
  , Part(..)
  , empty
  , addIngredient, addPart, addParts
  , scaleStep
  ) where

import           Business.Model.Ingredient             (Ingredient (..), scaleIngredient)
import qualified Business.Model.Quantities.Duration    as Q
import qualified Business.Model.Quantities.Mass        as Q
import qualified Business.Model.Quantities.Scalar      as Q
import qualified Business.Model.Quantities.Temperature as Q
import qualified Business.Model.Quantities.Volume      as Q
import           Data.Function                         ((&))
import           GHC.Generics


data Part
  = Text String
  | D Q.Duration
  | T Q.Temperature
  | V Q.Volume
  | M Q.Mass
  | P Q.Scalar
  deriving (Show, Read, Generic, Eq)


data Step
    = Step
    { ingredients :: [Ingredient]
    , parts       :: [Part]
    } deriving (Show, Read, Generic, Eq)

empty :: Step
empty = Step [] []

addIngredient :: Ingredient -> Step -> Step
addIngredient ingredient step = step { ingredients = ingredients step ++ [ingredient] }

addPart :: Part -> Step -> Step
addPart part step = step { parts = parts step ++ [part] }

addParts :: [Part] -> Step -> Step
addParts values step = step { parts = parts step ++ values }

scaleStep :: Float -> Step -> Step
scaleStep factor step
    = step { ingredients = ingredients step & map (scaleIngredient factor) }
