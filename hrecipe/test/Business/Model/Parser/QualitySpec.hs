module Business.Model.Parser.QualitySpec (spec) where


import qualified Business.Model.Parser.Helper   as Helper
import           Business.Model.Parser.Quantity
import           Test.Hspec                     (Spec, describe, it)
import           Test.Hspec.Megaparsec


spec :: Spec
spec = do

    describe "timeP should" $ do

        it "parse time in minutes" $ do
            Helper.parse timeP `shouldSucceedOn` "20 min"

        it "parse time in hours" $ do
            Helper.parse timeP `shouldSucceedOn` "1 h"

        it "parse time in hh:mm" $ do
            Helper.parse timeP `shouldSucceedOn` "01:30"

    describe "massP should" $ do

        it "parse mass in g" $ do
           Helper.parse massP `shouldSucceedOn` "2 g"

        it "parse mass in kg" $ do
           Helper.parse massP `shouldSucceedOn` "2 kg"

        it "parse mass in mg" $ do
           Helper.parse massP `shouldSucceedOn` "2 mg"

    describe "volumeP should" $ do

        it "parse volume in l" $ do
           Helper.parse volumeP `shouldSucceedOn` "2 l"

        it "parse volume in dl" $ do
           Helper.parse volumeP `shouldSucceedOn` "2 dl"

        it "parse volume in cl" $ do
           Helper.parse volumeP `shouldSucceedOn` "2 cl"

        it "parse volume in ml" $ do
           Helper.parse volumeP `shouldSucceedOn` "2 ml"

        it "parse volume in tsp" $ do
           Helper.parse volumeP `shouldSucceedOn` "2 tsp"

        it "volumeP should parse volume in tbs" $ do
           Helper.parse volumeP `shouldSucceedOn` "2 tbs"

    describe "scalarP should" $ do

        it "parse float" $ do
           Helper.parse scalarP `shouldSucceedOn` "2.3"

        it "parse integer" $ do
           Helper.parse scalarP `shouldSucceedOn` "2"

    describe "temperatureP should" $ do

        it "parse in celcius" $ do
           Helper.parse temperatureP `shouldSucceedOn` "200°C"

        it "parse in fahrenheit" $ do
           Helper.parse temperatureP `shouldSucceedOn` "200°F"
