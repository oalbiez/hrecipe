module Business.Model.Parser.Helper (parse) where


import           Business.Model.Parser.Common
import qualified Text.Megaparsec              as Megaparsec


parse :: Parser a -> String -> Either ParseError a
parse p = Megaparsec.parse p ""
