module Business.Model.Parser.CommonSpec (spec) where


import           Business.Model.Parser.Common
import qualified Business.Model.Parser.Helper as Helper
import           Test.Hspec                   (Spec, describe, it)
import           Test.Hspec.Megaparsec


spec :: Spec
spec = do

    it "integer should parse decimal number" $ do
       Helper.parse integer `shouldSucceedOn` "0"
       Helper.parse integer `shouldSucceedOn` "200"

    describe "float should" $ do

        it "parse number with comma" $ do
            Helper.parse float `shouldSucceedOn` "20.4"

        it "parse number without comma" $ do
            Helper.parse float `shouldSucceedOn` "20"

    it "keyword should parse symbol followed by ':'" $ do
       Helper.parse (keyword "paf") `shouldSucceedOn` "paf:"
       Helper.parse (keyword "paf") `shouldSucceedOn` "paf :"
       Helper.parse (keyword "paf") `shouldFailOn` "paf"

    describe "restOfLine should" $ do

        it "parse the end of line" $ do
            Helper.parse restOfLine `shouldSucceedOn` "polop and pilip"

        it "manage end of line comments" $ do
            Helper.parse restOfLine "polop and pilip # ignored" `shouldParse` "polop and pilip"

    it "restOfLineWithReference should parse the end of line" $ do
       Helper.parse restOfLineWithReference "polop" `shouldParse` ("polop", Nothing)
       Helper.parse restOfLineWithReference "polop # ignored" `shouldParse` ("polop", Nothing)
       Helper.parse restOfLineWithReference "polop @ref" `shouldParse` ("polop", Just "ref")
       Helper.parse restOfLineWithReference "polop @ref # ignored" `shouldParse` ("polop", Just "ref")
