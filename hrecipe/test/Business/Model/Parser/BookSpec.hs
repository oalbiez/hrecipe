module Business.Model.Parser.BookSpec (spec) where


import           Business.Model.Parser.Book
import qualified Business.Model.Parser.Helper       as Helper
import qualified Business.Model.Quantities.Duration as Q
import qualified Business.Model.Recipe              as R
import qualified Business.Model.Step                as S
import           Business.Model.Tag                 (Tag (Tag))
import qualified Data.Map                           as Map
import           Test.Hspec                         (Spec, describe, it)
import           Test.Hspec.Megaparsec


spec :: Spec
spec = do

    describe "qtyP should" $ do
        it "parse mass" $ do
            Helper.parse qtyP `shouldSucceedOn` "<100 g>"

        it "parse volume" $ do
            Helper.parse qtyP `shouldSucceedOn` "<50 cl>"

        it "parse piece" $ do
            Helper.parse qtyP `shouldSucceedOn` "<3>"

    describe "ingredientP should" $ do

        it "parse ingredients without aliment reference" $ do
            Helper.parse ingredientP `shouldSucceedOn` "<3> cucumbers"

        it "parse ingredients with aliment reference" $ do
            Helper.parse ingredientP `shouldSucceedOn` "<3> cucumbers @vegetable.cucumber/raw"

    describe "servingsP should" $ do

        it "parse servings count" $ do
            Helper.parse servingsP "servings: 4" `shouldParse` 4
            Helper.parse servingsP "servings : 4" `shouldParse` 4

        it "parse servings count with comment" $ do
            Helper.parse servingsP "servings: 4 # comment" `shouldParse` 4

    describe "sourceP should" $ do

        it "parse source" $ do
            Helper.parse sourceP "source: uri://source" `shouldParse` "uri://source"

        it "parse source with comment" $ do
            Helper.parse sourceP "source: uri://source # comment" `shouldParse` "uri://source"

    describe "tagsP should" $ do

        it "parse tags" $ do
            Helper.parse tagsP
                "tags: \n\
                \  - first\n\
                \  - second\n"
            `shouldParse` [Tag "first", Tag "second"]

        it "parse tags with comment" $ do
            Helper.parse tagsP
                "tags: # comment\n\
                \  - first  # comment\n\
                \  - second # comment\n"
            `shouldParse` [Tag "first", Tag "second"]

    describe "durationsP should" $ do

        it "parse durations" $ do
            Helper.parse durationsP
                "durations: \n\
                \  preparing: 40 min\n\
                \  cooking: 1 h\n"
            `shouldParse` Map.fromList [(R.Cooking, Q.minutes 60), (R.Preparing, Q.minutes 40)]

        it "parse durations with comment" $ do
            Helper.parse durationsP
                "durations: \n\
                \  waiting: 40 min # comment\n\
                \  baking: 1 h\n    # comment"
            `shouldParse` Map.fromList [(R.Baking, Q.minutes 60), (R.Waiting, Q.minutes 40)]

    describe "stepP should" $ do

        it "parse step wit simple description" $ do
            Helper.parse stepP
                "|>\n\
                \  Do something\n"
            `shouldParse` S.Step [] [S.Text "Do something"]

        it "parse step with quantity" $ do
            Helper.parse stepP
                "|>\n\
                \  Bake for [2h]\n"
            `shouldParse` S.Step [] [S.Text "Bake for", S.D (Q.hours 2)]

        it "parse step wit ingredients" $ do
            Helper.parse stepP
                "|>\n\
                \  <10g> of A\n\
                \  <50cl> of B\n\
                \  Do something\n"
            `parseSatisfies` ((== 2) . length . S.ingredients)

    describe "recipeP should" $ do

        it "parse simple recipe" $ do
            Helper.parse recipeP `shouldSucceedOn`
                "recipe: Name\n"

        it "parse recipe with reference" $ do
            Helper.parse recipeP `shouldSucceedOn`
                "recipe: Name @my.recipe\n"

        it "parse recipe with servings" $ do
            Helper.parse recipeP `shouldSucceedOn`
                "recipe: Name\
                \  servings: 6\n"

        it "parse recipe with source" $ do
            Helper.parse recipeP `shouldSucceedOn`
                "recipe: Name\
                \  source: uri://source\n"

        it "parse recipe with tags" $ do
            Helper.parse recipeP `shouldSucceedOn`
                "recipe: Name\n\
                \  tags:\n\
                \    - first\n\
                \    - second\n\
                \"

        it "parse recipe with durations" $ do
            Helper.parse recipeP `shouldSucceedOn`
                "recipe: Name\n\
                \  durations: \n\
                \    preparing: 40 min\n\
                \    cooking: 1 h\n\
                \"

        it "parse recipe with steps" $ do
            Helper.parse recipeP `shouldSucceedOn`
                "recipe: Name\n\
                \  |>\n\
                \    <10g> of A\n\
                \    <50cl> of B\n\
                \    Do something\n\
                \  |>\n\
                \    <100g> of C\n\
                \    Do something else\n\
                \  |>\n\
                \    Finish\n\
                \"

    describe "bookP should" $ do

        it "parse single recipe" $ do
            Helper.parse bookP `shouldSucceedOn`
                "recipe: Name\n"

        it "parse multiple recipes" $ do
            Helper.parse bookP `shouldSucceedOn`
                "recipe: First\n\
                \recipe: Second\n\
                \"
