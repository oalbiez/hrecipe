{-# LANGUAGE OverloadedStrings #-}
module Business.Model.Renderer.LatexSpec (spec) where


import           Business.Model.Book                   (Book (..))
import           Business.Model.Ingredient             (Ingredient (..), parseQty)
import           Business.Model.Quantities.Duration    (minutes)
import           Business.Model.Quantities.Scalar      as Q
import           Business.Model.Quantities.Temperature (celsius)
import           Business.Model.Recipe                 (DurationSpecification (..), Recipe (..))
import           Business.Model.Renderer.Context       (en, fr)
import           Business.Model.Renderer.Latex         (renderBook, renderIngredient, renderQuantity, renderRecipe,
                                                        renderStep)
import           Business.Model.Sample                 (creamedCucumbers)
import           Business.Model.Step                   (Part (..), Step (..))
import           Business.Model.Tag                    (Tag (Tag))
import qualified Data.Map                              as Map
import           Test.Hspec                            (Spec, it, shouldBe)


spec :: Spec
spec = do

    it "Latex render of quantity" $ do
        renderQuantity fr (parseQty "5 g") `shouldBe` "\\cunum{5.0}{g}"
        renderQuantity fr (parseQty "50 cl") `shouldBe` "\\cunum{50.0}{cl}"
        renderQuantity fr (parseQty "3") `shouldBe` "\\cuam{3.0}"

    it "Latex render of ingredient" $ do
        renderIngredient fr (Ingredient Nothing "Name" (parseQty "2")) `shouldBe` "\\Ingredient{\\cuam{2.0} Name}\n"

    it "Latex render of step with text" $ do
        renderStep fr (Step [] [Text "Do it"]) `shouldBe` "\\newstep\nDo it\n"

    it "Latex render of step with piece" $ do
        renderStep fr (Step [] [P (Q.scalar 3)]) `shouldBe` "\\newstep\n\\cuam{3.0}\n"

    it "Latex render of step with temperature" $ do
        renderStep fr (Step [] [T (celsius 180)]) `shouldBe` "\\newstep\n\\cunum{180}{C}\n"
        renderStep en (Step [] [T (celsius 180)]) `shouldBe` "\\newstep\n\\cunum{356}{F}\n"

    it "Latex render of step with duration" $ do
        renderStep fr (Step [] [D (minutes 10)]) `shouldBe` "\\newstep\n\\cunum{10}{min}\n"

    it "Latex render of step with full description" $ do
        renderStep fr (Step [] [Text "Cook during", D (minutes 10), Text "at", T (celsius 250)]) `shouldBe` "\\newstep\nCook during \\cunum{10}{min} at \\cunum{250}{C}\n"

    it "Latex render of step with ingredients" $ do
        renderStep fr (Step
            [Ingredient Nothing "Name" (parseQty "2")]
            [Text "Do it"])
            `shouldBe` "\\Ingredient{\\cuam{2.0} Name}\nDo it\n"

    it "Latex render of recipe" $ do
        renderRecipe fr (Recipe
            { identifier = Just "recipe"
            , name = "Name"
            , source = "uri://source"
            , serving = 4
            , durations = Map.fromList
                [ (Preparing, minutes 40)
                , (Waiting, minutes 10)
                , (Baking, minutes 30)
                , (Cooking, minutes 15)
                ]
            , tags = [Tag "a"]
            , steps =
                [ Step
                    { ingredients = [Ingredient Nothing "Stuff" (parseQty "2")]
                    , parts = [Text "Do it"]
                    }
                ]
            })
            `shouldBe` "\
                \\\begin{recipe}{Name}{\\serving{4}}{\\waiting{10}{min} \\preparing{40}{min} \\cooking{15}{min} \\baking{30}{min}}\n\
                \\\tags{\\tag{a}}\n\
                \\\Ingredient{\\cuam{2.0} Stuff}\n\
                \Do it\n\
                \\\end{recipe}\n"

    it "Latex render of book" $ do
        renderBook fr (Book {recipes = [creamedCucumbers] })
            `shouldBe` "\
                \\\documentclass[12pt]{article}\n\
                \\\usepackage[utf8]{inputenc}\n\
                \\\usepackage[french]{babel}\n\
                \\\usepackage[a4paper,textwidth=18cm,textheight=27cm]{geometry}\n\
                \\\usepackage[nonumber]{cuisine}\n\
                \\\usepackage{cooking-units}\n\
                \\\usepackage{fontawesome}\n\
                \\\usepackage{tikzsymbols}\n\
                \\n\\pagenumbering{gobble}\n\
                \\n\
                \\\newcommand{\\preparing}[2]{\\hspace{0.25cm} {\\faGears} {\\bfseries{\\cunum{#1}{#2}}}}\n\
                \\\newcommand{\\cooking}[2]{\\hspace{0.25cm} {\\fryingpan} {\\bfseries{\\cunum{#1}{#2}}}}\n\
                \\\newcommand{\\baking}[2]{\\hspace{0.25cm} {\\oven} {\\bfseries{\\cunum{#1}{#2}}}}\n\
                \\\newcommand{\\waiting}[2]{\\hspace{0.25cm} {attente} {\\bfseries{\\cunum{#1}{#2}}}}\n\
                \\\newcommand{\\serving}[1]{{\\faUser} #1}\n\
                \\\newcommand{\\tags}[1]{\\freeform #1}\n\
                \\\newcommand{\\tag}[1]{{\\small \\faHashtag}{\\bfseries #1}}\n\
                \\n\
                \\\RecipeWidths{\\textwidth}{3cm}{1cm}{4cm}{.75cm}{.75cm}\n\
                \\\renewcommand*{\\recipefont}{\\Large\\sffamily}\n\
                \\\renewcommand*{\\recipefreeformfont}{\\itshape}\n\
                \\\renewcommand*{\\recipeingredientfont}{\\large\\sffamily}\n\
                \\\renewcommand*{\\recipestepnumberfont}{\\LARGE\\bfseries\\sffamily}\n\
                \\\renewcommand*{\\recipetitlefont}{\\Huge\\selectfont\\bfseries\\sffamily}\n\
                \\n\
                \\\newcookingunit[c.\224.s]{cas}\n\
                \\\newcookingunit[c.\224.c]{cac}\n\
                \\n\
                \\\begin{document}\n\
                \\\begin{recipe}{Creamed Cucumbers}{\\serving{4}}{\\preparing{10}{min}}\n\
                \\\tags{}\n\
                \\\Ingredient{\\cuam{2.0} Cucumber}\n\
                \Cut the cucumber into slices.\n\
                \\n\
                \\\Ingredient{\\cunum{10.0}{cl} Soy cream}\n\
                \\\Ingredient{\\cuam{2.0} Lemon}\n\
                \Add the liquid cream and the lemon juice.\n\
                \\\end{recipe}\n\
                \\\end{document}\n"

