module Business.Model.Sample (creamedCucumbers) where


import           Business.Model.Ingredient          (Ingredient (..), Qty (..))
import           Business.Model.Quantities.Duration (minutes)
import qualified Business.Model.Quantities.Scalar   as Q
import qualified Business.Model.Quantities.Volume   as Q
import           Business.Model.Recipe              (DurationSpecification (..), Recipe (..))
import           Business.Model.Step                (Part (..), Step (..))
import qualified Data.Map                           as Map


cucumber :: Q.Scalar  -> Ingredient
cucumber qty = Ingredient (Just "vegetable.cucumber") "Cucumber" (Piece qty)

lemon :: Q.Scalar  -> Ingredient
lemon qty = Ingredient (Just "fruit.lemon") "Lemon" (Piece qty)

soyCream :: Q.Volume  -> Ingredient
soyCream qty = Ingredient (Just "cream.soy") "Soy cream" (Volume qty)

creamedCucumbers :: Recipe
creamedCucumbers
    = Recipe { identifier  = Just "creamed-cucumbers"
            , name = "Creamed Cucumbers"
            , source      = ""
            , serving     = 4
            , durations = Map.fromList
                [ (Preparing, minutes 10)
                ]
            , tags        = []
            , steps       = [ Step
                                [cucumber (Q.scalar 2)]
                                [Text "Cut the cucumber into slices."]
                            , Step
                                [soyCream (Q.cl 10), lemon (Q.scalar 2)]
                                [Text "Add the liquid cream and the lemon juice."]
                            ]
            }
