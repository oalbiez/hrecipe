module Business.Model.Quantities.MassSpec (spec) where


import           Business.Model.Quantities.Dimension (Dimension (..))
import           Business.Model.Quantities.Mass      (g, kg, mg, ug)
import           Test.Hspec                          (Spec, it, shouldBe)

spec :: Spec
spec = do

    it "Mass should render as µg" $
        render (ug 4) `shouldBe` "4.0 µg"

    it "Mass should render as mg" $ do
        render (mg 4) `shouldBe` "4.0 mg"
        render (ug 4000) `shouldBe` "4.0 mg"

    it "Mass should render as g" $ do
        render (g 10) `shouldBe` "10.0 g"
        render (mg 10000) `shouldBe` "10.0 g"
        render (ug 10000000) `shouldBe` "10.0 g"

    it "Mass should render as kg" $ do
        render (kg 10) `shouldBe` "10.0 kg"
        render (g 10000) `shouldBe` "10.0 kg"

    it "Mass should be addable" $
        render (g 1 ^+^ g 3) `shouldBe` "4.0 g"

    it "Mass should be scalable" $
        render (g 3 ^* 4.0) `shouldBe` "12.0 g"

    it "Masse should be instance of Eq" $ do
        g 3 == mg 3000 `shouldBe` True

    it "Masse should be instance of Ord" $ do
        g 4 > mg 3000 `shouldBe` True
