module Business.Model.Quantities.VolumeSpec (spec) where


import           Business.Model.Quantities.Dimension (Dimension (..))
import           Business.Model.Quantities.Volume    (cl, dl, l, ml, tbs, tsp)
import           Test.Hspec                          (Spec, it, shouldBe)


spec :: Spec
spec = do

    it "Volume should render as ml" $
        render (ml 1) `shouldBe` "1.0 ml"

    it "Volume should render as cl" $ do
        render (cl 50) `shouldBe` "50.0 cl"
        render (dl 5) `shouldBe` "50.0 cl"
        render (ml 500) `shouldBe` "50.0 cl"

    it "Volume should render as l" $ do
        render (l 2) `shouldBe` "2.0 l"
        render (dl 20) `shouldBe` "2.0 l"
        render (cl 200) `shouldBe` "2.0 l"
        render (ml 2000) `shouldBe` "2.0 l"

    it "Volume should render as tbs" $ do
        render (tbs 1) `shouldBe` "1.0 tbs"
        render (ml 15) `shouldBe` "1.0 tbs"
        render (ml 30) `shouldBe` "2.0 tbs"
        render (tsp 3) `shouldBe` "1.0 tbs"

    it "Volume should render as tsp" $ do
        render (tsp 1) `shouldBe` "1.0 tsp"
        render (ml 5) `shouldBe` "1.0 tsp"
        render (ml 10) `shouldBe` "2.0 tsp"

