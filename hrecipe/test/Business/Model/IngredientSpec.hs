module Business.Model.IngredientSpec (spec) where


import           Business.Model.Ingredient      (Ingredient (..), Qty (..), scaleIngredient)
import qualified Business.Model.Quantities.Mass as Q
import           Test.Hspec                     (Spec, it, shouldBe)


carrot :: Q.Mass  -> Ingredient
carrot qty = Ingredient (Just "vegetable.carrot") "Carrot" (Mass qty)

spec :: Spec
spec = do

    it "Ingredient should scale" $
        scaleIngredient 3.0 (carrot (Q.g 100)) `shouldBe` carrot (Q.g 300)
