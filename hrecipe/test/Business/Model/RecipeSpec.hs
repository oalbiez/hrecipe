module Business.Model.RecipeSpec (spec) where


import           Business.Model.Recipe (Recipe (..), scaleServing)
import           Business.Model.Sample (creamedCucumbers)
import           Test.Hspec            (Spec, it, shouldBe)


spec :: Spec
spec = do

    it "Recipe should scale" $
        let scaled = scaleServing 8 creamedCucumbers
        in serving scaled `shouldBe` 8
